/*
 * Copyright 2021 Arnaud Fonce <arnaud.fonce@r-w-x.net>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.rwx.netbeans.netesta.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import net.rwx.netbeans.netesta.GlobalActivation;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;

/**
 *
 * @author Arnaud Fonce <arnaud.fonce@r-w-x.net>
 */
@ActionID(category = "File", id = "net.rwx.netbeans.netesta.GlobalActionvationListener")
@ActionRegistration(displayName = "#netesta.disable")
@ActionReference(path = "UI/ToolActions/Files", position = 0)
public class GlobalActionvationListener extends AbstractAction {

    @Override
    public void actionPerformed(ActionEvent e) {
        if (GlobalActivation.isActivated()) {
            putValue("menuText", NbBundle.getMessage(GlobalActionvationListener.class, "netesta.enable"));
            GlobalActivation.desactivate();
        } else {
            putValue("menuText", NbBundle.getMessage(GlobalActionvationListener.class, "netesta.disable"));
            GlobalActivation.activate();
        }
    }
}
